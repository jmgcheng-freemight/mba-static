$(document).ready(function(){
	/*
		slick slider start
	*/
	$('.site-footer-carousel-block-slick').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 4,
		variableWidth: true
	});
	/*
		slick slider end
	*/
	
	/*
		fix menu start
	*/
	var o_main_site_header_menu = $('.site-menu.site-header-menu.main-site-header-menu');
	$(window).scroll(function(){
		o_scroll = $(window).scrollTop();
		
		if(o_scroll >= 100)
		{
			if( !o_main_site_header_menu.hasClass('position-fixed') )
			{
				o_main_site_header_menu.addClass('position-fixed');
			}
		}
		else
		{
			o_main_site_header_menu.removeClass('position-fixed');
		}
	});
	/*
		fix menu end
	*/
});