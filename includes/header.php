<!DOCTYPE html>
<html lang="ja">
	<head>
		<meta charset="UTF-8" />
		<meta name="keywords" content="" />
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, user-scalable=no" />
		
		<link rel="stylesheet" type="text/css" href="css/reset.css" />
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,500,700' rel='stylesheet' type='text/css' />
		<link href='https://fonts.googleapis.com/css?family=Libre+Baskerville:400,400italic' rel='stylesheet' type='text/css' />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="js/slick/slick.css"/>
		<link rel="stylesheet" type="text/css" href="js/slick/slick-theme.css"/>
		<!-- 
		-->
		<link rel="stylesheet" type="text/css" href="css/style.css" />
		
		<script type="text/javascript" src="js/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="js/slick/slick.min.js"></script>
		
		<script type="text/javascript" src="js/script.js"></script>
		
		<title>MBA</title>
	</head>
	<body>
		<header id="site-header" class="">
			<nav class="site-menu site-header-menu secondary-site-header-menu">
				<ul>
					<li>
						<span class="site-tel"><span class="glyph glyph-tel-white"></span>03-4405-8478</span>
					</li>
					<li>
						<a href="#"><span class="glyph glyph-consoltation-white"></span>無料面談</a>
					</li>
					<li>
						<a href="#"><span class="glyph glyph-catalog-white"></span>資料請求</a>
					</li>
					<li class="site-menu-item-last">
						<a href="#"><span class="glyph glyph-apply-white"></span>入学申し込み</a>
					</li>
					<div class="clear-both"></div>
				</ul>
				<div class="clear-both"></div>
			</nav>
			<nav class="site-menu site-header-menu main-site-header-menu">
				<ul>
					<li class="site-menu-item-first">
						<a class="site-logo" href="#">MBA</a>
					</li>
					<li>
						<a href="#">
							ABOUT
							<span class="sub-text">
								学校案内
							</span>
						</a>
					</li>
					<li>
						<a href="#">
							COURSE
							<span class="sub-text">
								コース紹介
							</span>
						</a>
					</li>
					<li>
						<a href="#">
							SCHEDULE
							<span class="sub-text">
								1日のスケジュール
							</span>
						</a>
					</li>
					<li>
						<a href="#">
							VOICE
							<span class="sub-text">
								卒業生の声<br/>
								〜Cross Story〜
							</span>
						</a>
					</li>
					<li>
						<a href="#">
							FLOW
							<span class="sub-text">
								留学前から留学後まで
							</span>
						</a>
					</li>
					<li>
						<a href="#">
							FAQ
							<span class="sub-text">
								よくあるご質問
							</span>
						</a>
					</li>
					<li class="site-menu-item-last">
						<a class="social-link item-first" href="#">
							<span class="glyph glyph-facebook-gray"></span>
						</a>
						<a class="social-link" href="#">
							<span class="glyph glyph-twitter-gray"></span>
						</a>
						<a class="social-link" href="#">
							<span class="glyph glyph-instagram-gray"></span>
						</a>
					</li>
					<div class="clear-both"></div>
				</ul>
				<div class="clear-both"></div>
			</nav>
		</header>
		
