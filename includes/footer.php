
		
		<footer id="site-footer" class="">
			<div class="wish-to-enroll-block">
				<div class="wish-to-enroll-blocks wish-to-enroll-block-1">
					<header>
						<h4><span class="glyph glyph-apply-blue"></span>入学をご希望の方</h4>
					</header>
					<p>
						入学をご希望の方は、入学申し込みフォームよりお手続きください。<br/>
						「MBAについてもっと知りたい」「入学について不安がある」という方は、無料面談や資料のご案内も行っております。
					</p>
				</div>
				<div class="wish-to-enroll-blocks wish-to-enroll-block-2">
					<div class="wish-to-enroll-action wish-to-enroll-action-col1">
						<a href="#"><span class="glyph glyph-apply-white"></span>入学を申し込む</a>
					</div>
					<div class="wish-to-enroll-action wish-to-enroll-action-col2">
						<a href="#"><span class="glyph glyph-arror-right-blue"></span>無料面談</a>
						<a href="#" class="item-last"><span class="glyph glyph-arror-right-blue"></span>資料請求</a>
						<div class="clear-both"></div>
					</div>
				</div>
				<div class="clear-both"></div>
			</div>
			<div class="inquiry-block">
				<div class="inquiry-blocks inquiry-block-1">
					<header>
						<h4><span class="glyph glyph-contacts-blue"></span>お問い合わせ</h4>
					</header>
					<p>
						ご入学に関してご不明な点がありましたら<br/>
						電話またはメールフォームよりお問い合わせください。
					</p>
				</div>
				<div class="inquiry-blocks inquiry-block-2">
					<div class="inquiry-block-action-col1">
						<p>
							<span class="glyph glyph-phone-blue"></span>03-4405-8478 <span class="action-date">（月〜金  10:00 - 19:00）</span>
						</p>
					</div>
					<div class="inquiry-block-action-col2">
						<a href="#"><span class="glyph glyph-mail-white"></span>メールでお問い合わせ</a>
					</div>
				</div>
				<div class="clear-both"></div>
			</div>
			
			<div class="site-footer-ads-block-wrapper">
				<div class="ads-block site-footer-ads-block">
					<img src="images/ad-pic-1.jpg" />
					<img src="images/ad-pic-2.jpg" class="item-last" />
				</div>
			</div>
			
			<div class="site-footer-carousel-block-wrapper">
				<div class="carousel-block site-footer-carousel-block">
					<div class="site-footer-carousel-block-slick">
						<div>
							<img src="images/footer-slide-img-1.jpg" />
						</div>
						<div>
							<img src="images/footer-slide-img-2.jpg" />
						</div>
						<div>
							<img src="images/footer-slide-img-3.jpg" />
						</div>
						<div>
							<img src="images/footer-slide-img-4.jpg" />
						</div>
						<div>
							<img src="images/footer-slide-img-1.jpg" />
						</div>
						<div>
							<img src="images/footer-slide-img-2.jpg" />
						</div>
						<div>
							<img src="images/footer-slide-img-3.jpg" />
						</div>
						<div>
							<img src="images/footer-slide-img-4.jpg" />
						</div>
					</div>
				</div>
			</div>
			
			<div class="site-footer-main-block-wrapper">
				<div class="site-footer-main-block">
					<nav class="site-menu site-footer-menu social-site-footer-menu">
						<ul>
							<li>
								<a href="#"><span class="glyph glyph-facebook-circle-gray"></span></a>
							</li>
							<li>
								<a href="#"><span class="glyph glyph-twitter-circle-gray"></span></a>
							</li>
							<li>
								<a href="#"><span class="glyph glyph-instagram-circle-gray"></span></a>
							</li>
							<div class="clear-both"></div>
						</ul>
					</nav>
					<nav class="site-menu site-footer-menu main-site-footer-menu">
						<ul>
							<li>
								<a href="#"><span class="glyph glyph-arrow-right-white"></span>会社概要</a>
							</li>
							<li>
								<a href="#"><span class="glyph glyph-arrow-right-white"></span>キャンセル規定</a>
							</li>
							<li class="item-last">
								<a href="#"><span class="glyph glyph-arrow-right-white"></span>免責事項</a>
							</li>
							<div class="clear-both"></div>
						</ul>
					</nav>
					<p>
						Copyright © United Re-Growth Co.Ltd All rights reserved.
					</p>
				</div>
			</div>
			
		</footer>
		
	</body>
</html>