<?php
	include('includes/header.php');
		
		
		
		$i_current_year = date("Y");
		$i_year_min = 1987;
		$i_year_max = $i_current_year + 10;
		$i_month_min = 1;
		$i_month_max = 12;
		$i_day_min = 1;
		$i_day_max = 31;
		
		
?>
		
		<div id="site-main" class="">
			
			
			<div class="site-content">
			
				<div class="ablk-1 apply">
					<header>
						<h2>
							APPLY
						</h2>
						<h6>
							入学申し込み
						</h6>
					</header>
					
					<div class="iblk-1-wrapper">
					
						<div class="iblk-1 form applicants">
							<p class="note">
								当校の受講約款を必ずお読みいただき、内容にご同意いただいた上で、下記フォームよりお手続きください。
							</p>
							<header>
								<h3>
									お申し込み内容
								</h3>
								<div class="accent-1"></div>
							</header>
							<ul class="data-list">
								<li class="li-item">
									<div class="col col-1">
										<p>
											入学希望日
										</p>
									</div>
									<div class="col col-2">
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													<select class="small">
														<?php
															for( $i_counter = $i_current_year;  $i_counter <= $i_year_max; $i_counter++ ):
														?>
															<option>
																<?php echo $i_counter; ?>
															</option>
														<?php
															endfor;
														?>
													</select>
													<select class="small">
														<?php
															for( $i_current_month = $i_month_min;  $i_current_month < $i_month_max; $i_current_month++ ):
														?>
															<option>
																<?php echo $i_current_month; ?>
															</option>
														<?php
															endfor;
														?>
													</select>
													<select class="small">
														<?php
															for( $i_current_day = $i_day_min;  $i_current_day < $i_day_max; $i_current_day++ ):
														?>
															<option>
																<?php echo $i_current_day; ?>
															</option>
														<?php
															endfor;
														?>
													</select>
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											希望コース
										</p>
									</div>
									<div class="col col-2">
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													<select class="medium">
														<option>
															選択してください
														</option>
														<option>
															(ゴールド)ビジネスマンツーマンコース
														</option>
														<option>
															(ゴールド)日常マンツーマンコース
														</option>
														<option>
															(ゴールド)TOEICコース
														</option>
														<option>
															(ゴールド)海外ワーカーコース
														</option>
														<option>
															(ゴールド)ペアコース
														</option>
														<option>
															(シルバー)ビジネスマンツーマンコース
														</option>
														<option>
															(シルバー)日常マンツーマンコース
														</option>
														<option>
															(シルバー)TOEICコース
														</option>
													</select>
												</p>
												<p class="note">
													ペアコースをお選びいただく場合には、備考欄にお二人様の希望コース（ビジネスマンツーマンコースor日常マンツーマンコースorTOEICコース）をご入力ください。
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											グループクラスを<br/>
											マンツーマンクラスに変更
										</p>
									</div>
									<div class="col col-2">
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													<input type="radio" name="opt_1" value="" checked="checked" id="radio1" /> <label class="label-radio" for="radio1">変更しない</label>
													<input type="radio" name="opt_1" value="" id="radio2" /> <label class="label-radio" for="radio2">1コマ分変更</label>
													<input type="radio" name="opt_1" value="" id="radio3" /> <label class="label-radio" for="radio3">2コマ分変更（MBA SILVERのみ）</label>
												</p>
												<p class="note">
													※1週間1コマ変更につき、+$100かかります
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											自由時間を<br/>
											マンツーマンクラスに変更
										</p>
									</div>
									<div class="col col-2">
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													<input type="radio" name="opt_2" value="" checked="checked" id="radio4" /> <label class="label-radio" for="radio4">変更しない</label>
													<input type="radio" name="opt_2" value="" id="radio5" /> <label class="label-radio" for="radio5">1コマ分変更</label>
													<input type="radio" name="opt_2" value="" id="radio6" /> <label class="label-radio" for="radio6">2コマ分変更</label>
												</p>
												<p class="note">
													※1週間1コマ変更につき、+$150かかります
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											部屋タイプ
										</p>
									</div>
									<div class="col col-2">
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													<input type="radio" name="opt_3" value="" checked="checked" id="radio7" /> <label class="label-radio" for="radio7">スタンダード</label>
													<input type="radio" name="opt_3" value="" id="radio8" /> <label class="label-radio" for="radio8">デラックス（１週間あたり＋$100）</label>
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											留学期間
										</p>
									</div>
									<div class="col col-2">
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													<select class="small">
														<?php
															for( $i_counter = 1;  $i_counter <= 24; $i_counter++ ):
														?>
															<option>
																<?php echo $i_counter; ?>週間
															</option>
														<?php
															endfor;
														?>
														<option>
															48週間
														</option>
													</select>
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											各種割引
										</p>
									</div>
									<div class="col col-2">
										<div class="row ">
											<div class="col col-1">
												<span class="pill-1 disabled">任意</span>
											</div>
											<div class="col col-2">
												<p>
													リピーター割引
												</p>
												<p>
													<input type="radio" name="opt_4" value="" checked="checked" id="radio9" /> <label class="label-radio" for="radio9">今回が初めてです</label>
													<input type="radio" name="opt_4" value="" id="radio10" /> <label class="label-radio" for="radio10">以前MBAに留学していました</label>
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1 disabled">任意</span>
											</div>
											<div class="col col-2">
												<p>
													紹介割引 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="note">紹介者のお名前をご入力ください。</span>
												</p>
												<p>
													<input class="medium" type="text" name="" value="" />
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											お支払い方法
										</p>
									</div>
									<div class="col col-2">
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													<input type="radio" name="opt_5" value="" checked="checked" id="radio11" /> <label class="label-radio" for="radio11">銀行振込</label>
													<input type="radio" name="opt_5" value="" id="radio12" /> <label class="label-radio" for="radio12">クレジットカード払い</label>
													<input type="radio" name="opt_5" value="" id="radio13" /> <label class="label-radio" for="radio13">PayPal</label>
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											備考欄
										</p>
									</div>
									<div class="col col-2">
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1 disabled">任意</span>
											</div>
											<div class="col col-2">
												<textarea class="large"></textarea>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
							</ul>
							
							<br/><br/><br/><br/>
							
							<header>
								<h3>
									基本情報
								</h3>
								<div class="accent-1"></div>
							</header>
							<ul class="data-list">
								<li class="li-item">
									<div class="col col-1">
										<p>
											お名前
										</p>
									</div>
									<div class="col col-2">
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													<input class="large" type="text" value="" />
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											パスポート記載名
										</p>
									</div>
									<div class="col col-2">
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													<input class="large" type="text" value="" />
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											会社名
										</p>
									</div>
									<div class="col col-2">
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1 disabled">任意</span>
											</div>
											<div class="col col-2">
												<p>
													<input class="large" type="text" value="" />
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											職種
										</p>
									</div>
									<div class="col col-2">
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1 disabled">任意</span>
											</div>
											<div class="col col-2">
												<p>
													<input class="large" type="text" value="" />
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											郵便番号
										</p>
									</div>
									<div class="col col-2">
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													<input class="large" type="text" value="" />
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											住所
										</p>
									</div>
									<div class="col col-2">
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													<input class="large" type="text" value="" />
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											電話番号
										</p>
									</div>
									<div class="col col-2">
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													<input class="large" type="text" value="" />
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											メールアドレス
										</p>
									</div>
									<div class="col col-2">
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													<input class="large" type="text" value="" />
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											緊急連絡先
										</p>
									</div>
									<div class="col col-2">
										<div class="row ">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													お名前
												</p>
												<p>
													<input class="large" type="text" value="" />
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
										<div class="row ">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													続柄
												</p>
												<p>
													<input class="large" type="text" value="" />
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													電話番号
												</p>
												<p>
													<input class="large" type="text" value="" />
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											性別
										</p>
									</div>
									<div class="col col-2">
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													<input type="radio" name="opt_6" value="" checked="checked" id="radio14" /> <label class="label-radio" for="radio14">男</label>
													<input type="radio" name="opt_6" value="" id="radio15" /> <label class="label-radio" for="radio15">女</label>
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											生年月日
										</p>
									</div>
									<div class="col col-2">
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													<select class="small">
														<?php
															for( $i_year_counter = $i_year_min;  $i_year_counter < $i_current_year; $i_year_counter++ ):
														?>
															<option>
																<?php echo $i_year_counter; ?>
															</option>
														<?php
															endfor;
														?>
													</select>
													<select class="small">
														<?php
															for( $i_current_month = $i_month_min;  $i_current_month < $i_month_max; $i_current_month++ ):
														?>
															<option>
																<?php echo $i_current_month; ?>
															</option>
														<?php
															endfor;
														?>
													</select>
													<select class="small">
														<?php
															for( $i_current_day = $i_day_min;  $i_current_day < $i_day_max; $i_current_day++ ):
														?>
															<option>
																<?php echo $i_current_day; ?>
															</option>
														<?php
															endfor;
														?>
													</select>
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											当校を知ったきっかけ
										</p>
									</div>
									<div class="col col-2">
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													<select class="medium">
														<option>
															選択してください
														</option>
														<option>
															(ゴールド)ビジネスマンツーマンコース
														</option>
														<option>
															(ゴールド)日常マンツーマンコース
														</option>
														<option>
															(ゴールド)TOEICコース
														</option>
														<option>
															(ゴールド)海外ワーカーコース
														</option>
														<option>
															(ゴールド)ペアコース
														</option>
														<option>
															(シルバー)ビジネスマンツーマンコース
														</option>
														<option>
															(シルバー)日常マンツーマンコース
														</option>
														<option>
															(シルバー)TOEICコース
														</option>
													</select>
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
								<li class="li-item">
									<div class="col col-1">
										<p>
											留学説明会の参加有無
										</p>
									</div>
									<div class="col col-2">
										<div class="row ">
											<div class="col col-1">
												<span class="pill-1">必須</span>
											</div>
											<div class="col col-2">
												<p>
													<input type="radio" name="opt_7" value="" checked="checked" id="radio16" /> <label class="label-radio" for="radio16">はい</label>
													<input type="radio" name="opt_7" value="" id="radio17" /> <label class="label-radio" for="radio17">いいえ</label>
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
										<div class="row last-item">
											<div class="col col-1">
												<span class="pill-1 disabled">任意</span>
											</div>
											<div class="col col-2">
												<p>
													参加日 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="note">上記に「はい」と答えた方のみご回答ください。</span>
												</p>
												<p>
													<select class="small">
														<?php
															for( $i_counter = $i_current_year;  $i_counter <= $i_year_max; $i_counter++ ):
														?>
															<option>
																<?php echo $i_counter; ?>
															</option>
														<?php
															endfor;
														?>
													</select>
													<select class="small">
														<?php
															for( $i_current_month = $i_month_min;  $i_current_month < $i_month_max; $i_current_month++ ):
														?>
															<option>
																<?php echo $i_current_month; ?>
															</option>
														<?php
															endfor;
														?>
													</select>
													<select class="small">
														<?php
															for( $i_current_day = $i_day_min;  $i_current_day < $i_day_max; $i_current_day++ ):
														?>
															<option>
																<?php echo $i_current_day; ?>
															</option>
														<?php
															endfor;
														?>
													</select>
												</p>
											</div>
											<div class="clear-both"></div>
										</div>
									</div>
									<div class="clear-both"></div>
								</li>
							</ul>
							
							<br/><br/>
							
							<div class="controls">
								<p class="agree">
									<input type="checkbox" name="" value="" />MBA受講約款に同意しました <br/>
								</p>
								<a class="anc-link-1" href="#">
									入力内容を確認する
								</a>
								<a class="anc-link-1 disabled" href="#">
									前の画面に戻る
								</a>
							</div>
							
							<br/><br/><br/><br/><br/><br/>
							
						</div>
						
						
						
						
					</div>
					
					
					
					
					
					
					
					
					
					
				</div>
			
			
			<div>
			
			
		</div>
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
<?php
	include('includes/footer.php');
?>