<?php
	include('includes/header.php');
?>
		
		<div id="site-main" class="">
			<div class="main-shoutout-wrapper">
				<div class="shoutout main-shoutout">
					<div class="shoutout-branding">
						<header>
							<h4>
								フィリピン留学
							</h4>
						</header>
						<div class="banner">
							<div class="banner-col banner-col1">
								<p>
									社会人シェア<br/>
									“圧倒的”
								</p>
							</div>
							<div class="banner-col banner-col2">
								<p>
									No.1
								</p>
							</div>
							<div class="clear-both"></div>
						</div>
					</div>
					<div class="shoutout-contents">
						<header>
							<h1>
								オトナの英語留学
							</h1>
						</header>
						<p>
							社会人の留学は 誰と 学ぶかで全てが変わる
						</p>
					</div>
					<div class="scroll-down shoutout-scroll-down">
						<p>
							<a href="#">
								SCROLL
								<br/>
								<span class="glyph glyph-arrow-down-white"></span>
							</a>
						</p>
					</div>
				</div>
			</div>
			
			<div class="featured-block main-featured-block">
				<header>
					<h2>フィリピン留学で唯一の、社会人のための語学学校です。</h2>
					<p>
						グローバル化が進んでいる今、社会人こそ語学習得が必要に迫られています。
					</p>
					<p>
						ビジネス現場で通用する英語を習得したい。いつか海外で活躍したい。
					</p>
					<p>					
						”オトナ留学MBA”では同じ目標をもつ様々な社会人との出会いがあります。
					</p>
					
				</header>
				<ul>
					<li class="featured-feature">
						<div>
							<img src="images/feature-img1.png" />
						</div>
						<header>
							<h5>MBAの特長</h5>
						</header>
						<p>他校とは全く違う！オトナ達の社交場</p>
						<a href="#"><span class="glyph glyph-arrow-right-white"></span> MBAの特長を見る</a>
					</li>
					<li class="featured-feature">
						<div>
							<img src="images/feature-img2.png" />
						</div>
						<header>
							<h5>選べる学習スタイル</h5>
						</header>
						<p>あなたに合わせたコースをご用意</p>
						<a href="#"><span class="glyph glyph-arrow-right-white"></span> コース紹介を見る</a>
					</li>
					<li class="featured-feature item-last">
						<div>
							<img src="images/feature-img3.png" />
						</div>
						<header>
							<h5>充実した生活環境</h5>
						</header>
						<p>学習環境だけでなく毎日の生活も充実</p>
						<a href="#"><span class="glyph glyph-arrow-right-white"></span> スケジュールを見る</a>
					</li>
					<div class="clear-both"></div>
				</ul>
				<div class="clear-both"></div>
			</div>
			
			<div class="voice-of-graduates-wrapper">
				<div class="voice-of-graduates">
					<header>
						<h2>VOICE <span class="font-baskerville">of</span> GRADUATES</h2>
					</header>
					<p>
						&nbsp;&nbsp;どのような経験をして、どのように生活が変化するのか。</br>
						実際に人生の分岐点を体験した卒業生の声をご紹介します。
					</p>
					<a href="#"><span class="glyph glyph-arrow-right-white"></span>卒業生の声 〜Cross Story〜</a>
				</div>
			</div>
			
			<div class="updates-block-wrapper">
				<div class="updates-block">
					<div class="news-block">
						<div class="news-information-block">
							<div class="news-information-blocks news-information-block-1">
								<h4>INFORMATION</h4>
								<p>お知らせ</p>
								<a href="#"><span class="glyph glyph-arrow-right-white"></span>MORE</a>
							</div>
							<div class="news-information-blocks news-information-block-2">
								<ul>
									<li class="news-information-item">
										<article>
											<div class="news-information-item-article-detail news-information-item-date">
												<p>2015.11.24</p>
											</div>
											<div class="news-information-item-article-detail news-information-item-headline">
												<p>【歳の数だけテキーラに挑戦！】AJITO店長「男・水田輝行」生誕祭！！</p>
											</div>
											<div class="news-information-item-article-detail news-information-item-link">
												<a href="#" class="link-blog">BLOG</a>
											</div>
											<div class="clear-both"></div>
										</article>
									</li>
									<li class="news-information-item">
										<article>
											<div class="news-information-item-article-detail news-information-item-date">
												<p>2015.11.10</p>
											</div>
											<div class="news-information-item-article-detail news-information-item-headline">
												<p>フィリピンのハロウィンパーティーが気合入りすぎな件</p>
											</div>
											<div class="news-information-item-article-detail news-information-item-link">
												<a href="#" class="link-blog">BLOG</a>
											</div>
											<div class="clear-both"></div>
										</article>
									</li>
									<li class="news-information-item item-last">
										<article>
											<div class="news-information-item-article-detail news-information-item-date">
												<p>2015.10.08</p>
											</div>
											<div class="news-information-item-article-detail news-information-item-headline">
												<p>11月キャンペーン【なんと、留学が１週間無料！！】</p>
											</div>
											<div class="news-information-item-article-detail news-information-item-link">
												<a href="#" class="link-news">NEWS</a>
											</div>
											<div class="clear-both"></div>
										</article>
									</li>
									<div class="clear-both"></div>
								</ul>
							</div>
							<div class="clear-both"></div>
						</div>
						<div class="news-media-block">
							<div class="news-media-blocks news-media-block-1">
								<h4>MEDIA RELEASE</h4>
								<p>メディア掲載</p>
								<a href="#"><span class="glyph glyph-arrow-right-white"></span>MORE</a>
							</div>
							<div class="news-media-blocks news-media-block-2">
								<ul>
									<li class="news-media-item">
										<article>
											<div class="news-media-item-article-blocks news-media-item-article-block-1">
												<a href="#">
													<img src="images/media-pic-1.gif" />
												</a>
											</div>
											<div class="news-media-item-article-blocks news-media-item-article-block-2">
												<header>
													<h6>2015.11.28</h6>
												</header>
												<p>
													<a href="#">
														Yahoo!Japanにて本校が紹介されました。
													</a>
												</p>
											</div>
											<div class="clear-both"></div>
										</article>
									</li>
									<li class="news-media-item">
										<article>
											<div class="news-media-item-article-blocks news-media-item-article-block-1">
												<a href="#">
													<img src="images/media-pic-2.gif" />
												</a>
											</div>
											<div class="news-media-item-article-blocks news-media-item-article-block-2">
												<header>
													<h6>2015.11.24</h6>
												</header>
												<p>
													<a href="#">
														TBSテレビのNews23にて本校が紹介されました。
													</a>
												</p>
											</div>
											<div class="clear-both"></div>
										</article>
									</li>
									<div class="clear-both"></div>
								</ul>
							</div>
							<div class="clear-both"></div>
						</div>
					</div>
					<div class="campaign-block">
						<header>
							<h3>CAMPAIGN</h3>
						</header>
						<article>
							<img src="images/campaign-pic-1.jpg" />
							<h4>
								<a href="#">
									『X’mas特別　Wキャンペーン』
								</a>
							</h4>
							<p>皆さんの日頃のご愛顧に感謝し、クリスマス特別Wキャンペーンを実施いたします！！</p>
							<div class="clear-both"></div>
						</article>
						<article>
							<img src="images/campaign-pic-2.jpg" />
							<h4>
								<a href="#">
									40分体験レッスン×フィリピン留学説明会 in 大阪
								</a>
							</h4>
							<p>社会人専門校として支持を頂いている”オトナ留学MBA”で生徒様より高い評価を得た人気講師陣が来日し...</p>
							<div class="clear-both"></div>
						</article>
					</div>
				</div>
			</div>
		</div>
		
<?php
	include('includes/footer.php');
?>